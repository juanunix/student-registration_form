package com.cse24gmail.jakir.studentinfo;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    Button btnSave;
    EditText etName;
    EditText etRegNo;
    EditText etEmail;
    Student student;
    DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

    }

    private void initialization() {
        etName= (EditText) findViewById(R.id.etName);
        etRegNo= (EditText) findViewById(R.id.etRegNo);
        etEmail= (EditText) findViewById(R.id.etEmail);

        student=new Student();
        dbAdapter=new DBAdapter(this);

        btnSave= (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=etName.getText().toString().trim();
                String regNo=etRegNo.getText().toString().trim();
                String email=etEmail.getText().toString().trim();
                if(name.isEmpty()){
                   if(regNo.isEmpty()){
                       if(email.isEmpty()){
                           Toast.makeText(getApplicationContext(),"Please Give All Information Carefully!",Toast.LENGTH_SHORT).show();
                           etName.setHintTextColor(Color.RED);
                           etRegNo.setHintTextColor(Color.RED);
                           etEmail.setHintTextColor(Color.RED);
                       }else{
                           Toast.makeText(getApplicationContext(),"Please Enter Your Name and Reg_NO!",Toast.LENGTH_SHORT).show();
                           etName.setHintTextColor(Color.RED);
                           etRegNo.setHintTextColor(Color.RED);
                       }

                   }else if(email.isEmpty()){
                       Toast.makeText(getApplicationContext(),"Please Enter Your Name and Email!",Toast.LENGTH_SHORT).show();
                       etName.setHintTextColor(Color.RED);
                       etEmail.setHintTextColor(Color.RED);
                   }else{
                       Toast.makeText(getApplicationContext(),"Please Enter Your Name!",Toast.LENGTH_SHORT).show();
                       etName.setHintTextColor(Color.RED);

                   }

                }else if(regNo.isEmpty()){
                    if(email.isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please Enter Your Reg_No. and Email!",Toast.LENGTH_SHORT).show();
                        etRegNo.setHintTextColor(Color.RED);
                        etEmail.setHintTextColor(Color.RED);

                    }else{
                        Toast.makeText(getApplicationContext(),"Please Enter Your Reg_No.!",Toast.LENGTH_SHORT).show();
                        etRegNo.setHintTextColor(Color.RED);
                    }
                }else if(email.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please Enter Your Email!",Toast.LENGTH_SHORT).show();
                    etEmail.setHintTextColor(Color.RED);

                }else{
                    dbAdapter.open();
                    Student student=new Student(name,regNo,email);
                    long inserted=dbAdapter.addStudent(student);
                    if(inserted>=0){
                        Toast.makeText(getApplicationContext(),"Data Saved On Database Successfully!",Toast.LENGTH_SHORT).show();
                    }
                    Intent intent=new Intent(MainActivity.this,SecondActivity.class);
                    startActivity(intent);

                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
